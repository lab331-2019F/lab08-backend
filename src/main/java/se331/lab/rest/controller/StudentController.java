package se331.lab.rest.controller;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.Student;
import java.util.ArrayList;
import java.util.List;
@Controller

public class StudentController {
    List<Student> students;

    public StudentController() {
        this.students = new ArrayList<>();

        this.students.add(Student.builder()
                .id(new Long(1l))
                .studentId("SE-001")
                .name("Prayuth")
                .surname("The minister")
                .gpa(3.59)
                .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/tu.jpg?alt=media&token=f16b5c1c-fbea-4d98-9fa3-732b69a1bae8")
                .penAmount(15)
                .description("The great man ever!!!!")
                .build());

        this.students.add(Student.builder().id(new Long(2l)).studentId("SE-002").name("Cherprang").surname("BNK48").gpa(4.01).image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/cherprang.png?alt=media&token=2e6a41f3-3bf0-4e42-ac6f-8b7516e24d92").penAmount(2).description("Code for Thailand").build());
        this.students.add(Student.builder().id(new Long(3l)).studentId("SE-099").name("Lucy").surname("FFF").gpa(4.0).image("https://scontent.fbkk8-2.fna.fbcdn.net/v/t1.0-9/10534585_567345183382942_2137359625930451218_n.jpg?_nc_cat=111&_nc_oc=AQmk5QNKBQiVEw4_GlkQmZkczCcrhuTCtfGnU2eDO414NX3VcUrKHUkPhTErzTvqL00&_nc_ht=scontent.fbkk8-2.fna&oh=5b5e32f674ea12f68b30fa2e953b69b8&oe=5E09FCBE").penAmount(3).description("Nice").build());


    }


    @GetMapping("/students")
    public ResponseEntity getAllStudent() {

        System.out.println(students);
        return ResponseEntity.ok(students);

    }
    @GetMapping("/students/{id}")
    public ResponseEntity getStudentById(@PathVariable("id")Long id) {
        //ResponseEntity.ok(studunt)
        return ResponseEntity.ok(students.get(Math.toIntExact(id-1)));
    }
    @PostMapping("/students")
    public ResponseEntity saveStudent(@RequestBody Student student){
        student.setId((long) this.students.size());
        this.students.add(student);
        return ResponseEntity.ok(student);
    }

}